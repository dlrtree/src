function y = orthCoreGen(r)
   d = length(r);

    y = rand(r);
    y = double(tens2mat(y, d)).';
    y = orth(y).';
    
    y = mat2tens(y, r, d);
end