clc; clear;

addpath('./tensor_toolbox');
addpath('./tensorlab_2016-03-28');    

% Parameters:
N = 2^4;
r = 5;

% Skew-symmetric matrix
global W
W = rand(r,r);
W = W - W.';
W = W / norm(W);

% Generate two random tensors:
Y0 = rndTensorTree(N,r);

h = [0.1, 0.01, 0.001];
T = 1;

for j=1:length(h)
    YY0 = Y0;
    t0 = 0;
    Max = T/h(j);
    for i=1:Max
        t1 = t0 + h(j);

        A1 = FF(t1,Y0);
        A0 = FF(t0,Y0);

        A1 = buildTensorTree(A1);
        A0 = buildTensorTree(A0);

        dA = A1 - A0;

        Y1 = RecursiveTTNIntegrator(YY0, dA, 0, 1); 
        YY0 = Y1;

        t0 = t1;

        %err(i) = norm( squeeze(A1) - buildTensorTree(Y1)) ./ norm(squeeze(A1));
        err(i,j) = norm( squeeze(A1) - buildTensorTree(Y1));
        t(i,j) = t1;

        fprintf("t = %f - err = %e\n", t1, err(i,j));
    end
end
    
t = [zeros(1,length(h)); t];
err = [eps*ones(1,length(h)); err];
 
for i=1:length(h)
    semilogy(t(:,i), err(:,i))
    hold on
end

function y = FF(t, X)
    global W
    
    Q = expm(t*W);
    
    % Connecting Tensors:
    X.Children{1}.Value = tmprod(X.Children{1}.Value, Q, 4);
    X.Children{2}.Value = tmprod(X.Children{2}.Value, Q, 3);
    
    % First Node:
    X.Children{1}.Children{1}.Value = X.Children{1}.Children{1}.Value*Q;    
    X.Children{1}.Children{2}.Value = X.Children{1}.Children{2}.Value*Q;   
    X.Children{1}.Children{3}.Value = X.Children{1}.Children{3}.Value*Q;
    
    % Second Node:
    X.Children{2}.Children{1}.Value = X.Children{2}.Children{1}.Value*Q;
    X.Children{2}.Children{2}.Value = X.Children{2}.Children{2}.Value*Q;
    
    % Third Node:
    X.Children{3}.Value = X.Children{3}.Value*Q;
    
    y = X;
end