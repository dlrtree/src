function y = retractionTensorTree(X, N, r)
    
    U1 = X.U{1};
    U1 = reshape(U1, [N N N r]);
    U1 = tucker_als( tensor(U1), [r r r r]);
    tmp = buildTucker(U1);
    tmp = tenmat( tensor(tmp), 4);
    tmp = double(tmp).';
    
    X.U{1} = tmp;
    
    U2 = X.U{2};
    U2 = reshape(U2, [N N r]);
    U2 = tucker_als( tensor(U2), [r r r r]);
    tmp = buildTucker(U2);
    tmp = tenmat( tensor(tmp), 3);
    tmp = double(tmp).';
    
    X.U{2} = tmp;
    
    y = X;
end