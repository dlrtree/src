function y = buildTensorTree(A)
    y = A.Value;
    

    if( ~isempty(A.Children))
        
        for i=1:length(A.Children)
       
            if( isempty(A.Children{i}.Children) )
                
                tmp = A.Children{i}.Value;
            else
                d = length(A.Children{i}.Children);

                tmp = buildTensorTree(A.Children{i});
                tmp = tenmat(tmp,d+1);
                tmp = double(tmp).';
            end
            
            y = ttm(tensor(y), tmp, i);
    
        end
    end
end

