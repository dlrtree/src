% Generate a random orthonormal Tensor Tree with
% given tree. Same N and rank for all the leaf and cores.
function Y = rndTensorTree(N, rr)
    n = [N, N, N, N, N, N];

    r = [rr,rr, rr];

    r1 = [rr,rr,rr];
    r2 = [rr,rr];
    r3 = rr;

    % First HT tensor:
    Y = Node;

    Y.Value = tensor(rand(r(1), r(2), r(3)), [r(1) r(2) r(3) 1]);
    Y.Children = {Node, Node, Node};

    % First Children:
    Y.Children{1}.Value = orthCoreGen([r1(1), r1(2), r1(3), r(1)]);
    Y.Children{1}.Children = {Node, Node, Node};

    Y.Children{1}.Children{1}.Value = orth( rand([n(1), r1(1)]) );
    Y.Children{1}.Children{1}.Children = [];

    Y.Children{1}.Children{2}.Value = orth( rand([n(2), r1(2)]) );
    Y.Children{1}.Children{2}.Children = [];

    Y.Children{1}.Children{3}.Value = orth( rand([n(3), r1(3)]) );
    Y.Children{1}.Children{3}.Children = [];

    % Second Children:
    Y.Children{2}.Value = orthCoreGen([r2(1), r2(2), r(2)]);
    Y.Children{2}.Children = {Node, Node};

    Y.Children{2}.Children{1}.Value =  orth( rand([n(4), r2(1)]) );
    Y.Children{2}.Children{1}.Children = [];

    Y.Children{2}.Children{2}.Value =  orth( rand([n(5), r2(2)]) );
    Y.Children{2}.Children{2}.Children = [];

    % Third Children :
    Y.Children{3}.Value = orth( rand([n(6), r3]) );
    Y.Children{3}.Children = [];
end