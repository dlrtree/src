function y = getElementTangentSpace(Y)

    
    % Core:
    tmp = Y;
    n = size(Y.Value);
    tmp.Value = tensor(rand(n), n);
    y = buildTensorTree(tmp);
    
    
    % First Children:
    tmp = Y;
    n = size(Y.Children{1}.Value);
    tmp.Children{1}.Value = orthCoreGen(n);
    y = y  + buildTensorTree(tmp);
    
        tmp = Y;
        n = size(Y.Children{1}.Children{1}.Value);
        tmp.Children{1}.Children{1}.Value = orth(rand(n));
        y = y  + buildTensorTree(tmp);

        tmp = Y;
        n = size(Y.Children{1}.Children{2}.Value);
        tmp.Children{1}.Children{2}.Value = orth(rand(n));
        y = y  + buildTensorTree(tmp);

        tmp = Y;
        n = size(Y.Children{1}.Children{3}.Value);
        tmp.Children{1}.Children{3}.Value = orth(rand(n));
        y = y  + buildTensorTree(tmp);
        
        
    % Second Children :
    tmp = Y;
    n = size(Y.Children{2}.Value);
    tmp.Children{2}.Value = orthCoreGen(n);
    y = y  + buildTensorTree(tmp);
    
        tmp = Y;
        n = size(Y.Children{2}.Children{1}.Value);
        tmp.Children{2}.Children{1}.Value = orth(rand(n));
        y = y  + buildTensorTree(tmp);

        tmp = Y;
        n = size(Y.Children{2}.Children{2}.Value);
        tmp.Children{2}.Children{2}.Value = orth(rand(n));
        y = y  + buildTensorTree(tmp);

    % Third Children :
    tmp = Y;
    n = size(Y.Children{3}.Value);
    tmp.Children{3}.Value = orth(rand(n));
    y = y  + buildTensorTree(tmp);

end