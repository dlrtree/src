function y = buildTucker(Z)
    
    d = length(size(Z));
    
    y = Z.core;
    for i=1:d
        y = ttm(y, Z.U{i}, i);
    end
   
end