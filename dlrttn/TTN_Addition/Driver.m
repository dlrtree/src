clc; clear;

addpath('./tensor_toolbox');
addpath('./tensorlab_2016-03-28');    

% Parameters:
N = 2^4;
r = 5;

% Generate two random tensors:
A0 = rndTensorTree(N,r);
A = buildTensorTree(A0);

% B0 = rndTensorTree(N,r);
% B0 = buildTensorTree(B0);

B0 = getElementTangentSpace(A0);
B0 = B0 ./ norm(B0);

index=1;
for h=[2^-1 2^-2 2^-3 2^-4 2^-5]
    
    B = h.* B0;
    
    % Recursive Tucker Approximation:
    Y1 = RecursiveTTNIntegrator(A0, B, 0, 1); 

    C = A + B;

    X = tucker_als(C, [r r r 1]);
    X = retractionTensorTree(X, N, r);

    X = buildTucker(X);

    C = squeeze(C);
    X = squeeze(X);

    Y1 = buildTensorTree(Y1);

    % Errors:
    err1(index) = norm(Y1-C);
    err2(index) = norm(Y1-X);
    err3(index) = norm(X-C);
    
    x(index) = h;
    
    index = index+1;

end

%% Plot
f1 = figure(1);
loglog(x, err1,'r -o','Linewidth',2, 'markers', 7);
hold on 
loglog(x, err2 ,'b --x','Linewidth',3, 'markers', 12);
hold on
loglog(x, err3 ,'g :*','Linewidth',3, 'markers', 5);
set(gca,'XDir','reverse');
ylabel('error');
xlabel('||B||');
legend({'$$||Y^1-C||$$', '$$||Y^1-X||$$', '$$||X-C||$$'}, 'interpreter', 'latex'); % Use this notation to be consistent with the notation in the paper: Y = Z_{1}, X = \tilde{Z}_1, C = D, ||A|| = ||B||
set(legend, 'interpreter','latex', 'fontsize', 15);
