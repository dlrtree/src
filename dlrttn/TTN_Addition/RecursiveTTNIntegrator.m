function A_1 = RecursiveTTNIntegrator(A_0, dA, t_0, t_1)
    A_1 = A_0;
    
    d = length(A_0.Children);
    
    % Costruzione punto iniziale Y_0:
    Y_0 = cell(1, d+1); 
    for i=1:d
        tmp = buildTensorTree(A_0.Children{i});
        
        dd = length(A_0.Children{i}.Children);
        
        if( length(size(tmp)) ~= 2) % it is not a matrix
            tmp = tenmat(tmp, dd+1);
            tmp = double(tmp).';
        end
        
        Y_0{i} = double(tmp);
    end
    
    Y_0{d+1} = A_0.Value;
    
    
    Y_1 = Y_0;
    C_0 = Y_0{end}; 
    r = size(C_0);
    
    for i=1:d
        
        C_0 = tenmat(C_0, i);
        C_0 = double(C_0).';

        [Q_0,S_0] = qr(C_0);
        Q_0 = Q_0(:, 1:r(i));
        S_0 = S_0(1:r(i), :);

        U_0 = Y_0{i};
        
        Q_0_T = mat2tens(Q_0.',r,i);

        tmp = tensor(Q_0_T);
        for l=i+1:d
            tmp = ttm(tmp, Y_0{l}, l);
        end
        V_0_T = tenmat(tmp,i);
        V_0_T = double(V_0_T);

        % First we calculate "F":
        val = tensor(dA);
        for k=1:(i-1)
            val = ttm(val, Y_1{k}.', k);
        end
        val = double(tenmat( val, i));  

        if( isempty(A_0.Children{i}.Children))      
            % It means that the node i is a leaf. We can update it directly

            % K-step:
                K_0 = U_0 * S_0.';
                K_1 = K_0 + (t_1-t_0)* val * V_0_T.';

                [U_1, SS_1] = qr(K_1);
                U_1 = U_1(:, 1:r(i));
                SS_1 = SS_1(1:r(i), :);
                
                A_1.Children{i}.Value = U_1;
  
        else
            
            % Update dA and core.
            dd = length(A_0.Children{i}.Children);
            
            A_0.Children{i}.Value = ttm( tensor(A_0.Children{i}.Value), S_0, dd+1);
            
            n = size( buildTensorTree(A_0.Children{i}) ); 
            
            tmp = ( val*V_0_T.' ).';
            newdA = mat2tens( tmp, n, dd+1);

            
            % Call again TTN integrator with new dA.
            result = RecursiveTTNIntegrator(A_0.Children{i}, newdA, t_0, t_1);
            A_1.Children{i} = result;
            
            % Update Core and U_1
            C = result.Value;
            rr = size(C);
            
            C = tenmat(C, dd+1);
            C = double(C);
            
            [C, S] = qr(C.', 0);
            
            C = C.';
            SS_1 = S;
            
            A_1.Children{i}.Value = mat2tens(C,rr, dd+1);
            
            tmp = buildTensorTree(A_1.Children{i});
            tmp = tenmat(tmp, dd+1);
            tmp = double(tmp).';
            
            U_1 = tmp;
     	end
        
            
        Y_1{i} = U_1;
        
        % S-step:       
            S_0 = SS_1;
            S_1 = S_0 - (t_1-t_0)*U_1.' * val * V_0_T.'; 

        % Re-initiziale the core :
            C_0 = S_1 * Q_0.';
            C_0 = mat2tens(C_0, r, i);
                
    end
    
    % Final Update of A_1.Value.
    L_0_T = tenmat(C_0, d);
    L_0_T = double(L_0_T);
    
    L_1_T = L_0_T +(t_1-t_0)* U_1.' * val;

    C_1 = mat2tens(L_1_T, r, d);
    
    Y_1{end} = C_1;
    
    A_1.Value = Y_1{end};
   
end