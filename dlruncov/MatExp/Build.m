function y = Build(X)
    U = X{1};
    V = X{2};
    S = X{3};
    
    y = U*S*V';
end