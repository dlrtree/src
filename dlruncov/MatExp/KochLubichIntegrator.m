% Koch-Lubich Integrator


index_h = 1;
for h = [   5*10^-1 2*10^-1 10^-1 ...
            5*10^-2 2*10^-2 10^-2 ... 
            5*10^-3 2*10^-3 10^-3 ...
            5*10^-4 2*10^-4 10^-4]
    
    index_r = 1;    
    for r=[ 4 8 16 32]
        UU0 = U0(:, 1:r);
        VV0 = V0(:, 1:r);
        SS0 = S0(1:r,1:r);
        
        Max = T/h;
        Y0 = {UU0, VV0, SS0};
        for i=1:Max
            fprintf('r = %d - t = %f', r, i*h);
            clc;
            
            Y1 = Method(Y0, (i-1)*h, i*h);
            Y0 = Y1;
        end
        
        Error(index_r, index_h) = norm( Build(Y1) - refsol, 'fro');
        
        if(isnan(Error(index_r, index_h)))
            Error(index_r, index_h) = Inf;
        end
        
        if(Error(index_r, index_h)>1)
            Error(index_r, index_h) = Inf;
        end
        
        index_r = index_r+1;
    end
    t(index_h) = h;
    index_h = index_h + 1;
end

run ErrorPlot


function Y1 = Method(Y0, t0, t1)
    U0 = Y0{1};
    V0 = Y0{2};
    S0 = Y0{3};
    
    h = t1-t0;
    
    N = size(U0,1);
    
    fun = @(X) G(X);
    
    %sol = myDopri(fun, vertcat(U0,S0), t0, t1);
    
    Y0 = vertcat(U0,V0,S0);
    
    k_1 = fun(Y0);
    k_2 = fun(Y0+0.5*h*k_1);
    k_3 = fun(Y0+0.5*h*k_2); 
    k_4 = fun(Y0+k_3*h);
    
    sol = Y0 + h*(1/6)*(k_1+2*k_2+2*k_3+k_4);
    
    U1 = sol(1:N, :);
    V1 = sol(N+1:2*N,:);
    S1 = sol(2*N+1:end,:);
    
    Y1 = {U1,V1, S1};
end

function y = G(X)

    N = size(X);
    
    r = N(2);
    N = N(1)-r; 
    N = N/2;
    
    U0 = X(1:N, :);
    V0 = X(N+1:2*N,:);
    S0 = X(2*N+1:end,:);
    
    Y0 = U0*S0*V0.';
    
    U1 = (eye(size(U0,1)) - U0*U0.')*F(Y0)*V0*inv(S0);
    V1 = (eye(size(V0,1)) - V0*V0.')'*F(Y0)'*U0*inv(S0)';
    S1 = U0.'*F(Y0)*V0;
    
    y = vertcat(U1,V1, S1);
end


function y = F(A)
    global W1 W2;
    
    y = W1*A + A*W2.' + A;
end