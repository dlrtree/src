% New method

index_h = 1;
for h = [   5*10^-1 2*10^-1 10^-1 ...
            5*10^-2 2*10^-2 10^-2 ...
            5*10^-3 2*10^-3 10^-3 ...
            5*10^-4 2*10^-4 10^-4]

    index_r = 1;
    
    for r=[ 4 8 16 32]
        UU0 = U0(:, 1:r);
        VV0 = V0(:, 1:r);
        SS0 = S0(1:r,1:r);
        
        Max = T/h;
        Y0 = {UU0,VV0, SS0};
        for i=1:Max
            fprintf('r = %d - t = %f', r, i*h);
            clc;
            
            Y1 = Method(Y0, (i-1)*h, i*h);
            Y0 = Y1;
            
        end
        
        Error(index_r, index_h) = norm( Build(Y1) - refsol, 'fro');
        index_r = index_r+1;
    end
    t(index_h) = h;
    index_h = index_h + 1;
end


run ErrorPlot


function Y1 = Method(Y0, t0, t1)
    U_0 = Y0{1};
    V_0 = Y0{2};
    S_0 = Y0{3};
    
    dA = A(t1) - A(t0);
    
    % K-step:
    K_0 = U_0*S_0;
    K_1 = K_0 + dA*V_0;
    
    [U_1, R] = qr(K_1,0);
    
    
    % K-step:
    K_0 = V_0*S_0';
    K_1 = K_0 + dA'*U_0;
    
    [V_1, R] = qr(K_1,0);
    
    
    % S-step:
    S_0 = (U_1'*U_0) * S_0 * (V_1'*V_0).';
    S_1 = S_0 + U_1.'*dA* V_1;
    
    Y1 = {U_1, V_1, S_1};    

end

