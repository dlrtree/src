function y = A(t)
    global W1 W2;
    
    N = size(W1,1);
    j = 1:N;
    D = 2.^-j;
    D = diag(D);
    
    expW1 = expm(t*W1);
    expW2 = expm(t*W2);
    expD = exp(t)*D;
    
    y = expW1 * expD * expW2.';
end