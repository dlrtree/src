function y = Ref(t,U0, V0, S0)
    global D
    global V_cos
    
    
    fun = @(X) D*X+X*D.'-V_cos*X*V_cos.';
    
    Y0 = U0*S0*V0.';
    y = myDopri(fun, Y0, 0, t);
end