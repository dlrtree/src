clc; clear;

run Init

subplot(1,3,1)

    T = .1;

    sol = Ref(T, U0, V0, S0);

    v = svd(sol);
    v = v(1:12);
    semilogy(v);

    hold on

subplot(1,3,2)

    run Test_New_rk2.m
    hold on
    
subplot(1,3,3)
    run Test_New_rk4.m
    hold on