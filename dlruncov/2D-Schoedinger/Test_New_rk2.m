% clc;
% 
% run Init

% T = .1;
% 
% sol = Ref(T, U0, V0, S0);
% 
% hold off
% 
% subplot(1,2,1);
% v = svd(sol);
% v = v(1:12);
% semilogy(v);
% pause(0.01);
% 
% hold on

index_h=1;
for h = [   10^-1 ...
            5*10^-2 2*10^-2 10^-2 ... 
            5*10^-3 2*10^-3 10^-3  ...
            5*10^-4 2*10^-4 10^-4 ] ... 
            %5*10^-5 2*10^-5 10^-5] ...
            %5*10^-6 2*10^-6 10^-6 ]

    index_r=1;
    for r= [4 6 8 10 12] 

        UU0 = U0(:,1:r);
        VV0 = V0(:,1:r);
        SS0 = S0(1:r, 1:r);
        
        Y0 ={UU0, VV0, SS0};
        
        Max = T/h;
        for i=1:Max
            clc;
            fprintf('r = %d, t = %f \n', r, i*h);
            
            Y1 = Method(Y0, (i-1)*h, i*h);
            Y0 = Y1;
            
        end 
        
        approxSol{index_r, index_h} = Y1;

        Error(index_r,index_h) = norm(sol - Build(Y1), 'fro');
        
        index_r = index_r+1;
    end
    
    t(index_h) = h;
    index_h=index_h+1;
end

%subplot(1,2,2)
run ErrorPlot

function Y1 = Method(Y0, t0, t1)
    
    global D
    global V_cos

    U_0 = Y0{1};
    V_0 = Y0{2};
    S_0 = Y0{3};
    
    h = t1-t0;
    
    % K1-step:
    funK = @(K) D*K + K*V_0.'*D.'*V_0 - V_cos*K*V_0.'*V_cos*V_0;
    
    K_0 = U_0*S_0;
    K_1 = rk2(funK, K_0, h);
    
    [U_1, R] = qr(K_1,0);
    
    % K2-step:
    funK = @(K) D*K + K*U_0.'*D.'*U_0 -  V_cos*K*U_0.'*V_cos*U_0;
    
    K_0 = V_0*S_0.';
    K_1 = rk2(funK, K_0, h);
    
    [V_1, R] = qr(K_1,0);
    
    
    % S-step:
    funS = @(S) U_1.'*D*U_1*S + S*V_1.'*D.'*V_1 - (U_1.'*V_cos*U_1)*S*(V_1.'*V_cos*V_1);
    
    S_0 = (U_1'*U_0) * S_0 * (V_1'*V_0).';
    S_1 = rk2(funS, S_0, h);
    
    Y1 = {U_1, V_1, S_1};    

end

function y = Build(X)
    U = X{1};
    V = X{2};
    S = X{3};
    
    y = U*S*V.';
end
