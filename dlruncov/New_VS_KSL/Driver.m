

T = 5;
L = 15;
M = 1i*eye(2);
A = [ 2, -1;
     -1,  3];
Nr = 20;

err_new = zeros(Nr ,2);
err_ksl = zeros(Nr ,2);
 


n = 128;

dx = L/n;
x = (-L/2:dx:L/2-dx).';
y = x-1;
one = 1+0*x;


N = 50000;
h = T/N;


Y0 = pi^(-0.5) * exp(0.5i*(M(1,1)*(x.*x)*one' + (M(1,2)+M(2,1))*(x*y') + M(2,2)*one*(y.*y)'));

disp('Reference');
Yref = refsolver(Y0, L, A, h, N);


disp('h = 0.01, n = 128');

N = 1000;
h = T/N;
     
for r = 1:Nr

    disp(['r = ' num2str(r)]);
    
    Y_new1 = Newsolver(Y0, L, A, r, h, N);
    Y_ksl = solver(Y0, L, A, r, h, N);

    err_new(r, 1) = norm(Yref-Y_new1, 'fro') * dx;
    err_ksl(r, 1) = norm(Yref-Y_ksl, 'fro') * dx; 
    
    
    disp(['error = ' num2str(err_new(r, 1))]);
    disp(['error = ' num2str(err_ksl(r, 1))]);
end



% N = 250;
% h = T/N;
% 
% disp('h = 0.1, n = 128');
% 
% for r = 1:Nr
% 
%     disp(['r = ' num2str(r)]);
%     Y = solver(Y0, L, A, r, h, N);
% 
%     err(r, 2) = norm(Y-Yref, 'fro') * dx;
%     disp(['error = ' num2str(err(r, 2))]);
% end



% n = 64;
% 
% dx = L/n;
% x = (-L/2:dx:L/2-dx).';
% y = x-1;
% one = 1+0*x;
% 
% 
% Yref2 = Yref(1:2:end, 1:2:end);
% Y02   =   Y0(1:2:end, 1:2:end);
% 
% Yref3 = refsolver(Y02, L, A, h, N);
% 
% N = 500;
% h = T/N;
% 
% 
% disp('h = 0.01, n = 64');
%      
% for r = 1:Nr
% 
%     disp(['r = ' num2str(r)]);
%     Y = solver(Y02, L, A, r, h, N);
% 
%     err(r, 3) = norm(Y-Yref2, 'fro') * dx;
%     disp(['error = ' num2str(err(r, 3))]);
% end
% 
% 
% 
% N = 250;
% h = T/N;
% 
% disp('h = 0.1, n = 64');
% 
% for r = 1:Nr
% 
%     disp(['r = ' num2str(r)]);
%     Y = solver(Y02, L, A, r, h, N);
% 
%     err(r, 4) = norm(Y-Yref2, 'fro') * dx;
%     disp(['error = ' num2str(err(r, 4))]);
% end


close all;
figure1 = figure();
axes1 = axes('Parent', figure1, 'LineWidth', 2, 'Xscale', 'lin', ...
             'Yscale', 'log', 'XTick', 2:2:Nr, 'Xlim', [0, 21], ...
             'YTick', 10.^(-10:2:0), 'Ylim', [1e-10, 1], 'FontSize', 24);
box(axes1,'on');
hold(axes1,'all');

semilogy(1:Nr, err_new(:,1), '-x', 1:Nr, err_new(:,2), '--d', ...
         'Parent', axes1, 'LineWidth', 2, 'MarkerSize', 10);
% 1:Nr, err(:,2), 'k-x', 1:Nr, err(:,4), 'k--x', ...


semilogy(1:Nr, err_ksl(:,1), '-x', 1:Nr, err_ksl(:,2), '--d', ...
         'Parent', axes1, 'LineWidth', 2, 'MarkerSize', 10);

ylabel('error');
xlabel('rank');

%print('-deps', 'tdse-nolabel');
%system('ps2pdf -dSubsetFonts=true -dEmbedAllFonts=true -dEPSCrop tdse-nolabel.eps tdse-nolabel.pdf');
%system('pdftops -eps tdse-nolabel.pdf');


















% SKR"AP.

% a = pi^(-0.5);
% q = [0; 1];
% S = 0;
% p = [0; 0];
% 
% [a, q, S, p, M] = rk4param(a, q, S, p, M, A, h, N);

% xgb = x - q(1);
% ygb = x - q(2);
% Ygb = a * ...
%       exp(0.5i*(M(1,1)*(xgb.*xgb)*one' + (M(1,2)+M(2,1))*(xgb*ygb') + M(2,2)*one*(ygb.*ygb)') + ...
%           1i*(p(1)*(xgb*one') + p(2)*(one*ygb')) + 1i*S);



% J = [zeros(2),   eye(2);
%            -A, zeros(2)];
% 
% qp = expm(T*J) * [0; 1; 0; 0];
% QP = expm(T*J) * [eye(2); 1i*eye(2)];
% 
% Q = QP(1:2,:);
% P = QP(3:4,:);
% MM = P/Q;







