function U = arnoldi(U, H, dt)

    % Lanczos parameters.
    iter      = 20;
    tol       = 1e-10;
    
    [n, r] = size(U);
    
    Vp = zeros(n,r,iter+1);      % ON-basis vectors.
    normu = norm(U, 'fro');
    Vp(:,:,1) = U / normu;
    A = [];

    for j = 1:iter
        Vp(:,:,j+1) = H(Vp(:,:,j));

        h = reshape(Vp(:,:,1:j),n*r,j)' * reshape(Vp(:,:,j+1), n*r,1);
        Vp(:,:,j+1) = Vp(:,:,j+1) - reshape(reshape(Vp(:,:,1:j),n*r,j)*h,n,r,1);
        h1 = reshape(Vp(:,:,1:j),n*r,j)' * reshape(Vp(:,:,j+1),n*r,1);
        Vp(:,:,j+1) = Vp(:,:,j+1) - reshape(reshape(Vp(:,:,1:j),n*r,j)*h1,n,r,1);
        gamma = norm(Vp(:,:,j+1),'fro');
        Vp(:,:,j+1) = Vp(:,:,j+1)/gamma;

        A = [A, h+h1;
             zeros(1,j-1), gamma];

        [V1, D] = eig(A(1:j, 1:j));
        temp = V1 * diag(exp(-1i*dt*diag(D))) / V1;
        residual = abs(dt * A(j+1,j) * temp(j,1));
        if residual < tol
            break;
        end
    end
    if residual > tol
        disp('Arnoldi not converging.');
    end
    
    
    U = reshape(reshape(Vp(:,:,1:j),n*r,j) * temp * eye(j, 1) * normu, n, r);

    
