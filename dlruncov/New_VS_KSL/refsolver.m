function Y = refsolver(Y, L, A, h, N)

    [n,~] = size(Y);
    dx = L/n;
    x = (-L/2:dx:L/2-dx).';
    one = 1+0*x;
    
    kk = -(2*pi/L)^2 * ([0:(0.5*n-1), -0.5*n:-1].^2).';
    KK = repmat(kk, 1, n);

    % Harmonic oscillator
    V1 = 0.5 * [x.*x, x, one];
    V2 = (diag([A(1,1), A(1,2)+A(2,1), A(2,2)]) * [one, x, x.*x]')';
    
    % Henon--Heiles
%     V1 = [one, x.*x];
%     lambda = 0.111803;
%     A = [0, 0, 0.5, -lambda/3;
%          0.5, lambda, 0, 0];
%     V2 = (A * [one, x, x.*x, x.*x.*x]')';

    V = V1 * V2';

    f = @(Y) -0.5 * ifft(KK .* fft(Y)) - 0.5 * ifft(KK .* fft(Y.')).' + V .* Y;

%     f = @(Y) ifft(KK .* fft(Y)) + ifft(KK .* fft(Y'))' + V .* Y;
    
    for k = 1:N

        Y = arnoldi(Y, f, h);

%         surf(x, x, real(Y));
%         axis([-L/2, L/2, -L/2, L/2, -1, 1]);
%         drawnow;

    end
    

end
