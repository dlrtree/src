function Up = hadaprod(U, u)

    [n, R] = size(U);
    [~, r] = size(u);

    if R > n

        [R, n] = size(U);
        [r, ~] = size(u);
   
        Up = zeros(R*r, n);

        for j = 0:R-1

            Up(j*r + (1:r), :) = repmat(U(j+1,:), r, 1) .* u;
        end

    else

        Up = zeros(n, R*r);

        for j = 0:R-1

            Up(:, j*r + (1:r)) = repmat(U(:,j+1), 1, r) .* u;
        end
    end
end
