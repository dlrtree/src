function Y = solver(Y0, L, A, r, h, N)

    [n,~] = size(Y0);
    dx = L/n;
    x = (-L/2:dx:L/2-dx).';
    one = 1+0*x;
    
    kk = -(2*pi/L)^2 * ([0:(0.5*n-1), -0.5*n:-1].^2).';
    KK = repmat(-0.5*kk, 1, r);

    % Harmonic oscillator
    V1 = 0.5 * [x.*x, x, one];
    V2 = (diag([A(1,1), A(1,2)+A(2,1), A(2,2)]) * [one, x, x.*x]')';
    
    % Henon--Heiles
%     V1 = [one, x.*x];
%     lambda = 0.111803;
%     A = [0, 0, 0.5, -lambda/3;
%          0.5, lambda, 0, 0];
%     V2 = (A * [one, x, x.*x, x.*x.*x]')';



    [U, S, V] = svd(Y0);
    U = U(:, 1:r);
    S = S(1:r, 1:r);
    V = V(:,1:r);
    

    f1p = @(US, V) ifft(KK .* fft(US)) + US * ifft(KK .* fft(V))' * V + ...
                   hadaprod(V1, US) * hadaprod(V2', V') * V;
    f1m = @(S, U, V) - U' * ifft(KK .* fft(U)) * S - S * ifft(KK .* fft(V))' * V + ...
                     - U' * hadaprod(V1, U*S) * hadaprod(V2', V') * V;
    f2p = @(SVt, U) U' * ifft(KK .* fft(U)) * SVt + ifft(KK .* fft(SVt'))' + ...
                    U' * hadaprod(V1, U) * hadaprod(V2', SVt);


    for k = 1:N

        
        % First order splitting.
        % 1. (US)' = A'V; V' = 0.
        K = arnoldi(U*S, @(z) f1p(z, V), h);
        [U, S] = qr(K, 0);

        % 2. S' = - U^TA'V; U' = 0; V' = 0.
        S = arnoldi(S, @(z) f1m(z, U, V), h);

        % 3. (VS^T)' = A'^TU; U' = 0.
%         K = arnoldi(V*S', @(z) f2p(z, U), h);
%         [V, S] = qr(K, 0);
%         S = S';
        K = arnoldi(S*V', @(z) f2p(z, U), h);
        [V, S] = qr(K', 0);
        S = S';
        
        Y = U * S * V';

%         surf(x, x, real(Y));
%         axis([-L/2, L/2, -L/2, L/2, -1, 1]);
%         drawnow;

    end
    

end
