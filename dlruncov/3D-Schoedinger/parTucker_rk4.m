C0 = CC0(1:r,1:r,1:r);
U1 = UU1(:,1:r);
U2 = UU2(:,1:r);
U3 = UU3(:,1:r);

Y0 = {U1,U2,U3,C0};

%% low-rank solution:
Max = T/h;
t = 0; 
Y1 = Y0;
for i=1:Max
    
    Y1 = Method(Y1, h);

    fprintf("t = %f - rank = %d \n", i*h, r);
    
end

    tmp = buildTucker(Y1);
    err = norm(ref(:) - tmp(:)); 

%% Methods
function Y1 = Method(Y0,h)

    U01 = Y0{1};
    U02 = Y0{2};
    U03 = Y0{3};
    C0 = Y0{end};
        
    % K-step:
    U1 = phi(1, Y0, h);
    U2 = phi(2, Y0, h);
    U3 = phi(3, Y0, h);
    
    %% S-step:
    TT1 = U1'*U01;
    TT2 = U2'*U02;
    TT3 = U3'*U03;
    
    C0 = buildTucker({TT1, TT2, TT3, C0});
    C1 = rk4(C0, @(C) funC(U1,U2,U3,C), h);
    %C1 = rk4(C0, @(C) funC(U1,U2,U3,C), h);
    %C1 = TensorODE_Solver(C0, @(t,C) funC(U1,U2,U3,C), 0, h);
    
    
    %C1 = C1 ./ norm( C1(:) );
    Y1 = {U1,U2,U3,C1};

end

function y = funC(U1,U2,U3,C)

    y = buildTucker({U1,U2,U3,C});
    y = HH(y);
    y = buildTucker({U1',U2', U3', y});

end

function y = phi(i, Y0,h)
    %find the rank:
    r = size(Y0{end});
    r = r(i);

    %build init value and matrix V0:
%     Y0 = buildTucker(Y0);
%     Y0 = tens2mat(Y0,i);
%     [U0,S0,V0] = svd(Y0,0);
%     
%     U0 = U0(:,1:r);
%     V0 = V0(:,1:r);
%     S0 = S0(1:r,1:r);
    
    U0 = Y0{i};
    C0 = Y0{end};
    
    C = tens2mat(C0,i);
    [Q,S0] = qr(C.',0);
    S0 = S0.';    
    
    Q = mat2tens(Q.', size(C0),i);
    
    switch i
        case 1
            V0 = tmprod(Q, {Y0{2}, Y0{3}}, [2 3]);            
        case 2
            V0 = tmprod(Q, {Y0{1}, Y0{3}}, [1 3]);
        case 3
            V0 = tmprod(Q, {Y0{1}, Y0{2}}, [1 2]);
    end
    
    V0 = tens2mat(V0,i)';
    
    %solve the K problem:
    K0 = U0*S0;
    K1 = rk4(K0, @(K) funK(K, V0, i), h);
    %K1 = rk4(K0, @(K) funK(K, V0, i), h);
    %K1 = MatrixODE_Solver(K0, @(t,K) funK(K, V0, i), 0, h);
    
    
    [U1,R]=qr(K1);
    U1 = U1(:,1:r);
    
    y = U1;
end

function y = funK(X, V0, i)
    K = size(X,1);
    
    y = X*V0';
    y = mat2tens(y, [K K K],i);
    y = HH(y);
    y = tens2mat(y,i)*V0;
end