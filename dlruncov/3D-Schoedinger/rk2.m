function Y = rk2(Y,f, h)
     k1 = h*f(Y);
     k2 = h*f(Y+h*k1);

 
     Y = Y + 0.5*(k1+k2);   

end