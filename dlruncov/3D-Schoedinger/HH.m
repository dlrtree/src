function Y = HH(X)
    global d
    global D
 	global V_cos
    
    Y  = zeros(size(X));
    for i=1:d
        Y = Y + 0.5*tmprod(X,D,i);
    end
    
    Y = Y - tmprod(X,{V_cos, V_cos, V_cos}, [1 2 3]);
    
end