function Y = rk1(Y,f, h)
     k1 = h*f(Y);

 
     Y = Y + k1;   

end