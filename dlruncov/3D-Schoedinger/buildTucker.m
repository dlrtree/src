function y = buildTucker(Z)
    U_1 = Z{1};
    U_2 = Z{2};
    U_3 = Z{3};

    S_0 = Z{end};
    
    Y_0 = tmprod(S_0, {U_1, U_2, U_3}, [1 2 3]);
    
    y = Y_0;
end
