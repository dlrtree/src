clc;clear;

addpath('./tensorlab');

K=100;  
T=.1;

global d
d=3; %number of particles

global V_cos
global D

%% Build Potential Matrices:
D = -K/2 : K/2-1;
dx = (2*pi*K^-1); 
x = dx.*D;

D = full(gallery('tridiag',K,-1,2,-1));

V_cos = diag(1- cos(x));

%% Initial Data:

CC0 = zeros(K,K,K); 

for i=1:K
   CC0(i,i,i) = 10^-i;
end

UU1 = orth(rand(K,K));
UU2 = orth(rand(K,K));
UU3 = orth(rand(K,K));
