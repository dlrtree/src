%% reference solution

YY0 = {UU1,UU2,UU3,CC0};

fprintf("Computing reference solution....\n");

Max = T/h;
ref = buildTucker(YY0);
for i=1:Max
    
    fprintf("t = %f \n", i*h); 
    
    ref = TensorODE_Solver(ref, @(t,X) HH(X), 0, h);
end