run Init_Tucker.m

h = 1e-2;
run refSol.m

v = svd(tens2mat(ref,1));
v = v(1:12);

subplot(1,3,1)
    semilogy(v); 
    pause(0.1)

hold on

tmp = [.05 .02 .01];
tt = tmp;
for n=1:2
    tmp = tmp/10;
    tt = [tt, tmp];
end
tt = [T, tt];

index_h = 1; 
for h = tt
        
        index_r = 1;
        for r = [4 6 8 10 12]
            run parTucker_rk2.m

            error_table(index_h, index_r) = err(end,1);

            index_r = index_r+1;
            clc;
        end

        index_h = index_h + 1;
    

end

subplot(1,3,2)
run ErrorPlot.m
hold on

index_h = 1; 
for h = tt
        
        index_r = 1;
        for r = [4 6 8 10 12]
            run parTucker_rk4.m

            error_table(index_h, index_r) = err(end,1);

            index_r = index_r+1;
            clc;
        end

        index_h = index_h + 1;
    

end

subplot(1,3,3)
run ErrorPlot.m
hold on

