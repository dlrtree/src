clc; clear; 
hold off

N = 100; T=1;

global W
W = rand(N,N);
W = W - W.';

U0 = eye(N);
S0 = A(0);

refsol = A(T);

index_h = 1;
for h = [   5*10^-1 2*10^-1 10^-1 ...
            5*10^-2 2*10^-2 10^-2 ... 
            5*10^-3 2*10^-3 10^-3 ...
            5*10^-4 2*10^-4 10^-4]
    
    index_r = 1;    
    for r=[ 4 8 16 32]
        UU0 = U0(:, 1:r);
        SS0 = S0(1:r,1:r);
        
        Max = T/h;
        Y0 = {UU0, SS0};
        for i=1:Max
            fprintf('r = %d - t = %f', r, i*h);
            clc;
            
            Y1 = Method(Y0, (i-1)*h, i*h);
            Y0 = Y1;
        end
        
        Error(index_r, index_h) = norm( Build(Y1) - refsol, 'fro');
        
        if(isnan(Error(index_r, index_h)))
            Error(index_r, index_h) = Inf;
        end
        
        if(Error(index_r, index_h)>1)
            Error(index_r, index_h) = Inf;
        end
        
        index_r = index_r+1;
    end
    t(index_h) = h;
    index_h = index_h + 1;
end

run ErrorPlot


function Y1 = Method(Y0, t0, t1)
    U0 = Y0{1};
    S0 = Y0{2};
    
    h = t1-t0;
    
    N = size(U0,1);
    
    fun = @(X) G(X);
    
    %sol = myDopri(fun, vertcat(U0,S0), t0, t1);
    
    Y0 = vertcat(U0,S0);
    
    k_1 = fun(Y0);
    k_2 = fun(Y0+0.5*h*k_1);
    k_3 = fun(Y0+0.5*h*k_2); 
    k_4 = fun(Y0+k_3*h);
    
    sol = Y0 + h*(1/6)*(k_1+2*k_2+2*k_3+k_4);
    
    U1 = sol(1:N, :);
    S1 = sol(N+1:end,:);
    
    Y1 = {U1, S1};
end

function y = G(X)

    N = size(X);
    
    r = N(2);
    N = N(1)-r;
    
    U0 = X(1:N, :);
    S0 = X(N+1:end,:);
    
    Y0 = U0*S0*U0.';
    
    U1 = (eye(size(U0,1)) - U0*U0.')*F(Y0)*U0*inv(S0);
    S1 = U0.'*F(Y0)*U0;
    
    y = vertcat(U1, S1);
end


function y = F(A)
    global W
    
    y = W*A + A*W.' + A;
end


function y = A(t)
    global W;
    
    N = size(W,1);
    j = 1:N;
    D = 2.^-j;
    D = diag(D);
    
    expW = expm(t*W);
    expD = exp(t)*D;
    
    y = expW * expD * expW.';
end


function y = Build(X)
    U = X{1};
    S = X{2};
    
    y = U*S*U.';
end