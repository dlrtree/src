clc;clear;

addpath('./expokit');
addpath('./tensorlab');

K=2^7;  r=10;
%h=1e-3; T=1;

load('GroundState_r5_K128_T40_e-5.mat')

global V_cos
global V_sin

%% Build Potential Matrices:

D = -K/2 : K/2-1;
dx = (2*pi*K^-1); 
x = dx.*D;

V_cos = diag(cos(x));
V_sin = diag(sin(x));

%% Fourier Matrix
global Fourier
global iFourier

Fourier = zeros(K,K);
 
for i=1:K
    for j=1:K
        cost = 2*pi/K;
        Fourier(i,j) = exp(1i*cost*(i-K/2-1)*(j-K/2-1));
    end
end

%iFourier = inv(Fourier);
iFourier = K^-1.* Fourier';

%% Initial Data:
C_0 = zeros(r,r,r);

for i=1:r
    for j=(i+1):r
        for k=(j+1):r
            num = rand() +1i*rand();
            C_0(i,j,k) =  num;
            C_0(j,i,k) = -num;
            C_0(j,k,i) = num;
            C_0(i,k,j) = -num;
            C_0(k,i,j) = num;
            C_0(k,j,i) = -num;
        end
    end
end
clear i j k num

C_0 = C_0 ./ norm( C_0(:) );
U_0 = orth(rand(K,r));

