clc;
clear Energy
clear Error

run Init

h=1e-3; T=1;

Y_0 = GroundState;

Max = T/h;
for i=1:Max
    
    [E, Y_1] = Method(Y_0,i*h, h);
    Energy(i) = E;
    
    Y_0 = Y_1;
    
    % Reference Solution, h=10e-6, rank5:
%     if( mod(i,100) == 0)
%         RS = Y_1;
%         j = i / 100;
%         str = 'ReferenceSolution/ReferenceSolution_';
%         save(strcat(str,num2str(j)), 'RS');
%     end
    
    % Error
%      j = i*h*10^4;
%      str = 'ReferenceSolution/ReferenceSolution_';
%      load(strcat(str,num2str(j)));
%      RS = Build(RS);
%      Y_1 = Build(Y_1);

%      Error(i) = dx*norm(RS(:)-Y_1(:));
    
    clc;
    fprintf("r = %d - t = %f - E = %f. \n", r, i*h, Energy(i));

    pause(0.0001);
    plot(h:h:i*h, Energy)
    
end



function [E, Y_1] = Method(Y_0,t,h)
    global V_cos
    global V_sin
    
    global Fourier
    global iFourier

    U_0 = Y_0{1};
    C_0 = Y_0{end};
    
    K = size(U_0,1);
    r = size(U_0,2);
    
    D1 = -K/2 : K/2-1;
    D1 = -omega(t)*D1;
    D1 = diag(D1);
    
    D = -K/2 : K/2-1;
    D = 0.5 .* D.^2;
    D = diag(D);
    
    D3 = 0.5*omega(t)^2 * eye(K);
    
   
    
    %% Split Fourier : Kinetic + Cos_Potential
    expV = expm(+1i*h/2*V_cos);
    expT = expm(-1i*h*D);
    
    U_0 = expV*U_0;

    U_0 = Fourier*U_0;
    U_0 = expT*U_0;
    U_0 = expm(-1i*h*D1)*U_0;
    U_0 = expm(-1i*h*D3)*U_0;
    U_0 = iFourier*U_0;

    U_0 = expV*U_0;
    
    %% Double Potential part - dlrsym
    % K-step:
    C_0 = tens2mat(C_0,1);
    [Q_0, S_0_T] = qr(C_0.',0);
    S_0 = S_0_T.';
    
    K_0 = U_0*S_0;
    
    K_1 = Prop_K(h, K_0, Q_0, U_0);
    
    % Qr Decomposition:
    [U_1, R] = qr(K_1, 0);
    
    %% S-step:
    C_0 = Y_0{end};
    
    TT = U_1'*U_0;
    
    C_0 = Build({TT, C_0});
    
    C_1 = Prop_C(h, C_0, U_1);
    
    C_1 = AntiSymmetrize(C_1);
    
    C_1 = C_1 ./ norm(C_1(:));
    
    
    %% Calculate Energy:
    Y_1 = {U_1, C_1};
    
    dx = 2*pi*K^-1;
    
    E = dx^2 * real(DiscreteEnergy(t+h, Y_1));

end

function y = Operator_K(t, K, Q, U)
    global V_cos
    global V_sin
    global Fourier
    global iFourier

    N = size(U,1);
    r = size(U,2);
    
    
    y = - V_cos * K ...
        - K * Q.'*kron(U.'*V_cos*conj(U), eye(r))*conj(Q) ...
        - K * Q.'*kron(eye(r), U.'*V_cos*conj(U))*conj(Q) ...
        + V_cos * K * Q.'*kron(U.'*V_cos*conj(U), eye(r))*conj(Q) ...
        + V_sin * K * Q.'*kron(U.'*V_sin*conj(U), eye(r))*conj(Q) ...
        + V_cos * K * Q.'*kron(eye(r), U.'*V_cos*conj(U))*conj(Q) ...
        + V_sin * K * Q.'*kron(eye(r), U.'*V_sin*conj(U))*conj(Q) ...
        + K * Q.'*kron(U.'*V_cos*conj(U), U.'*V_cos*conj(U))*conj(Q) ...
        + K * Q.'*kron(U.'*V_sin*conj(U), U.'*V_sin*conj(U))*conj(Q);

end

function C1 = Prop_C(h, C0, U)
    global V_cos
    global V_sin

    N = size(U,1);
    r = size(U,2);

    Vc = U'*V_cos*U;
    Vs = U'*V_sin*U;
    
    fun = @(C) ...
        + tmprod(C, {Vc, Vc}, [1 2]) + tmprod(C, {Vs, Vs}, [1 2]) ...
        + tmprod(C, {Vc, Vc}, [1 3]) + tmprod(C, {Vs, Vs}, [1 3]) ...
        + tmprod(C, {Vc, Vc}, [2 3]) + tmprod(C, {Vs, Vs}, [2 3]);
    
     C = zeros(r^3, r^3);

    index=1;
    for i=1:r
        for j=1:r^2

            E_ij = zeros(r,r^2);
            E_ij(i,j) = 1;
            
            E_ij = mat2tens(E_ij, [r r r], 1);

            T = fun(E_ij);
            
            T = tens2mat(T,1);

            v = reshape(T, [r^3 1]);

            C(:, index) = v;

            index = index+1;
        end
    end
    
    y0 = tens2mat(C0, 1);
    
    y0 = reshape(y0.', [r^3 1]);
    
    %y1 = expm(-h*C)*y0;
    y1 = expv(-1i*h, C, y0);
    
    y1 = reshape(y1, [r r r]);
    
    C1 = y1;
end

function K1 = Prop_K(h, K0, Q, U)
    global V_cos
    global V_sin

    N = size(U,1);
    r = size(U,2);
    
    
    M1 = Q.'*kron(U.'*V_cos*conj(U), eye(r))*conj(Q);
    
    M2 = Q.'*kron(U.'*V_sin*conj(U), eye(r))*conj(Q);
    
    M3 = Q.'*kron(eye(r), U.'*V_cos*conj(U))*conj(Q);
    
    M4 = Q.'*kron(eye(r), U.'*V_sin*conj(U))*conj(Q);
    
    M5 = Q.'*kron(U.'*V_cos*conj(U), U.'*V_cos*conj(U))*conj(Q);
    
    M6 = Q.'*kron(U.'*V_sin*conj(U), U.'*V_sin*conj(U))*conj(Q);
    
    
    fun = @(K) ...
        + V_cos * K * M1 ...
        + V_sin * K * M2 ...
        + V_cos * K * M3 ...
        + V_sin * K * M4 ...
        + K * M5 ...
        + K * M6;
    
    C = zeros(N*r, N*r);

    index=1;
    for i=1:N
        for j=1:r

            E_ij = zeros(N,r);
            E_ij(i,j) = 1;

            T = fun(E_ij);

            v = reshape(T, [N*r 1]);

            C(:, index) = v;

            index = index+1;
        end
    end
    
    y0 = reshape(K0, [r*N 1]);
    
    %y1 = expm(-h*C)*y0;
    y1 = expv(-1i*h, C, y0);
    
    K1 = reshape(y1, [N r]);  

end

function y = DiscreteEnergy(t, Y)
    global V_cos
    global V_sin
    global Fourier
    global iFourier
    
    K = size(V_cos, 1);
    
    D = -K/2 : (K/2-1);
    D = 0.5*D.^2;
    D = diag(D);
    
    D1 = -K/2 : K/2-1;
    D1 = -omega(t)*D1;
    D1 = diag(D1);
    
    D3 = 0.5*omega(t)^2 * eye(K);
    
    D = iFourier*D*Fourier;
    
    D1 = iFourier*D1*Fourier;
      
    D3 = iFourier*D3*Fourier;
    
    D = D + D1 + D3;

    U = Y{1};
    C = Y{2};
    
    tmp = tens2mat(C,1);
    
    y = 0;
    y = y + 3*trace( tmp*tmp' * U'*D*U);
    y = y - 3*trace( tmp*tmp' * U'*V_cos*U);
    
    tmp2 = U.'*V_cos*conj(U);
    tmp2 = kron(tmp2, tmp2);
    
    y = y + 3*trace( tmp'*tmp * tmp2 );
    
    tmp2 = U.'*V_sin*conj(U);
    tmp2 = kron(tmp2, tmp2);
    
    y = y + 3*trace( tmp'*tmp * tmp2 );

end

function y = omega(t)
    I_0 = 100;
    %omeg_ = .1;
    %tau = 2*pi*100;
    
    %y = I_0*exp(-(t*10.^3).^2 ./ tau.^2) .* sin( omeg_ .* (t*10.^3) );
    
    omeg_ = 100;
    tau = 0.2 * pi;
    y = I_0 * exp(-(t).^2 ./ tau.^2) .* sin( omeg_ .* (t) );
end

function C_1 = AntiSymmetrize(C_0)
     
    r = size(C_0,1);
    
    C_1 = zeros(r,r,r);
    for i=1:r
        for j=(i+1):r
            for k=(j+1):r
                num = C_0(i,j,k);
                C_1(i,j,k) =  num;
                C_1(j,i,k) = -num;
                C_1(j,k,i) = num;
                C_1(i,k,j) = -num;
                C_1(k,i,j) = num;
                C_1(k,j,i) = -num;
            end
        end
    end
    
end

