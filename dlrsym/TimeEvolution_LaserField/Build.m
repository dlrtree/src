function y = Build(Z)
    U_0 = Z{1};
    S_0 = Z{end};

    %Y_0 = tmprod(S_0, U_0, 1);
    %Y_0 = tmprod(Y_0, U_0, 2);
    %Y_0 = tmprod(Y_0, U_0, 3);
    
    Y_0 = tmprod(S_0, {U_0, U_0, U_0}, [1 2 3]);
    
    y = Y_0;
end