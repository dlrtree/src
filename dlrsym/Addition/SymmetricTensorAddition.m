% Addition of 3d symmetric tensors.

%d = 3;  % dimension of the problem
n = 100;   % modes
r = 10;    % ranks

%% Initialization

% Construct Symemtric low-rank tensor A with orthogonal basis matrices U
S0 = rand([r r r]); S0 = symmetrize(tensor(S0)); 
S1 = rand([r r r]); S1 = symmetrize(tensor(S1));
SkewU = rand(n); 
SkewU = SkewU-SkewU';

S = S0 + S1;
U = expm(SkewU)*eye(n,r);

A = tucker(S,U);

% Construct a tangent tensor B
DU = randn(n,r);
DS = randn(n,n,n);

dU = DU(:,1:r);
dS = DS(1:r,1:r,1:r);
dS = symmetrize(tensor(dS));

B = tucker(dS,U) + tucker2(S,dU,U,U) + tucker2(S,U,dU,U) + tucker2(S,U,U,dU);

% Orthonormalize B. Since dividing a Tucker tensor by a scalar is not
% defined in the Toolbox, we proceed in the following way:
Bfull = full(B);
Bnorm = norm(B);
BB = Bfull/Bnorm;
B0 = tucker(BB, eye(n)); % normalized Tucker tensor 

% We let the norm of the increment B be smaller and smaller
Nnorm = 5;
norms = 2.^(-(1:Nnorm));

erryc = zeros(1, Nnorm);
errxc = zeros(1, Nnorm);
erryx = zeros(1, Nnorm);

for k = 1:Nnorm

        B = norms(k) * B0;
        
        % direct addition
        C = A + B; 
       
        % apply the KSTucker integrator to perform addition
        Y = KSTucker(S, U, B);
        
        % Best rank-r Symmetric approximation
        X = tucker_als(C,r);
        X = tucker_sym(X,r);
        X = tensor(X);
        
        % Compute the errors
        erryc(k) = norm(Y-C);
        erryx(k) = norm(Y-X);
        errxc(k) = norm(X-C);
end

%% Plot
f1 = figure(1);
loglog(norms, erryc,'r -o','Linewidth',2, 'markers', 7);
hold on 
loglog(norms, erryx ,'b --x','Linewidth',3, 'markers', 12);
hold on
loglog(norms, errxc ,'g :*','Linewidth',3, 'markers', 5);
set(gca,'XDir','reverse');
ylabel('error');
xlabel('||B||');
legend({'$$||Y^1-C||$$', '$$||Y^1-X||$$', '$$||X-C||$$'}, 'interpreter', 'latex'); % Use this notation to be consistent with the notation in the paper: Y = Z_{1}, X = \tilde{Z}_1, C = D, ||A|| = ||B||
set(legend, 'interpreter','latex', 'fontsize', 15);
%print(f1, 'tensoraddition.pdf');

%% Functions

function y = tucker(S,U)
    S = tensor(S);
    
    tmp = ttm(S, U, 1);
    tmp = ttm(tmp, U, 2);
    tmp = ttm(tmp, U, 3);
    
    y = tmp;
end

function y = tucker2(S,U,V,W)
    S = tensor(S);
    
    tmp = ttm(S, U, 1);
    tmp = ttm(tmp, V, 2);
    tmp = ttm(tmp, W, 3);
    
    y = tmp;
end

function y = KSTucker(C_0, U_0, B)
    %KS Numerical Integrator : 

    % Calcolo di U_1 : 
    tmp = double(tenmat(C_0,1));
    [Q_0, S_0] = qr(tmp.',0);

    tmp = kron(U_0.', U_0.');
    V = Q_0.' * tmp;

    tmp = double(tenmat(B,1));
    K_1 = U_0*S_0.' + tmp * V.';

    [U_1, R] = qr(K_1,0);


    % Calcolo del core :
    T = U_1'*U_0;
    E = tucker(C_0, T);
    D = tucker(B, U_1');

    C_1 = E + D;

    % Approx finale :
    Y_1 = tucker(C_1, U_1);


    y = Y_1;
end