clc; clear;

run Init 

run program_GroundState_Fermionic.m
run program_GroundState_Fermionic_withoutAntisymmetry.m
run program_GroundState_Bosonic.m


%% Plot
t = h : h : T;

hold off

plot(t, Energy_GroundState_Fermionic)
hold on
plot(t, Energy_GroundState_Fermionic_withoutAntisymmetry)
hold on
plot(t, Energy_GroundState_Bosonic)
hold off