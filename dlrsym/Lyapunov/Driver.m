% Normalize data and represents them in same scale.

clear; clc;

load('./data/data_simulation.mat') % these data are obtained running the program Test_symLyapunov.m present in the folder.

% Remove some rank error:
Error(end, :) = []; %rank16 not needed.
Error(end, :) = []; %rank14 not needed.
Error(1, :) = [];   %rank2 not needed.

MaxRank = 12;

hold off

subplot(1,2,1)

tmp = svd(sol);
tmp = tmp(1:MaxRank);

C = tmp(1);

semilogy(tmp./C);
hold on


subplot(1,2,2) 
Error = Error ./ C;
run ErrorPlot