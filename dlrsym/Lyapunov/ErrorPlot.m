
%t = [ 10^-1 10^-2 10^-3];
%t = [10^-1 10^-2 10^-3 10^-4];

loglog_conv(t, Error);

function loglog_conv(vect,err)

    [n1 n2]=size(err);

    symbols='sox.d+^*v><';
    ms=[6 6 6 8 6 6 6 6 6 6 6];
     gr=(linspace(.66,0,n1))';
     colors=[gr gr gr];

    for jj=1:n1
        loglog(vect,err(jj,:), ...
               'LineWidth',1,...
               'Marker',symbols(jj),...
               'MarkerSize',ms(jj),...
               'MarkerIndices', 1:1:length(vect), ...
               'MarkerSize',12,...
               'Color', colors(jj,:));
        if jj==1
            hold on;
        end
    end
    hold off;
end

