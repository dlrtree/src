clc;

run Init

T = .1;

sol = Ref(T, U0 , S0);

hold off

subplot(1,2,1);
semilogy(svd(sol));
pause(0.01);

hold on

index_h=1;
for h = [   10^-1 ...
            5*10^-2 2*10^-2 10^-2 ... 
            5*10^-3 2*10^-3 10^-3 ...
            5*10^-4 2*10^-4 10^-4 ... 
            5*10^-5 2*10^-5 10^-5] ...
            %5*10^-6 2*10^-6 10^-6 ]

    index_r=1;
    for r= [2 4 6 8 10 12 14 16] 

        UU0 = U0(:,1:r);
        SS0 = S0(1:r, 1:r);
        
        Y0 ={UU0, SS0};
        
        Max = T/h;
        for i=1:Max
            clc;
            fprintf('r = %d, t = %f \n', r, i*h);
            
            Y1 = Method(Y0, (i-1)*h, i*h);
            Y0 = Y1;
            
        end 
        
        approxSol{index_r, index_h} = Y1;

        Error(index_r,index_h) = norm(sol - Build(Y1), 'fro');
        
        index_r = index_r+1;
    end
    
    t(index_h) = h;
    index_h=index_h+1;
end

subplot(1,2,2)

run ErrorPlot

function y = Ref(t,U0, S0)
    global D
    global Q
    
    
    fun = @(X) D*X+X*D.'+Q;
    y = myDopri(fun, U0*S0*U0.', 0, t);
end

function Y1 = Method(Y0, t0, t1)
    
    global D
    global Q

    U_0 = Y0{1};
    S_0 = Y0{2};
    
    h = t1-t0;
    
    % K-step:
    funK = @(K) D*K + K*U_0.'*D.'*U_0 + Q*U_0;
    
    K_0 = U_0*S_0;
    K_1 = myDopri(funK, K_0, 0, h);
    
    [U_1, R] = qr(K_1,0);
    
    % S-step:
    funS = @(S) U_1.'*D*U_1*S + S*U_1.'*D.'*U_1 + U_1.'*Q*U_1;
    
    S_0 = (U_1'*U_0) * S_0 * (U_1'*U_0).';
    S_1 = myDopri(funS, S_0, 0, h);
    
    Y1 = {U_1, S_1};    

end

function y = Build(X)
    U = X{1};
    S = X{2};
    
    y = U*S*U.';
end
