clc; clear;

d=10;
N = d^2;
T = .1; 

%% Matrices

global D

D = full(gallery('tridiag',d,-1,2,-1));
D = kron(D, eye(size(D))) + kron(eye(size(D)), D);

global Q

Q = randLowRankMat(N, 5);

%% Initial Data:
U0 = orth(rand(N,N));

S0 = zeros(N,N);
S0(1,1) = 1;

function y=randLowRankMat(N, r)

    U0 = orth(rand(N,r));
    S0 = rand(r,r);
    y = (U0*S0)*(U0*S0).';
end
